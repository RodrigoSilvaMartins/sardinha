﻿<%-- 
ETML
Auteur: Ricardo Cardoso Oliveira & Rodrigo Silva Martins
Date: 21.05.2018
Description: Récupère tout les liens contenu dans les balise <a>   
--%>

<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MainPage.aspx.cs" Inherits="Sardinha.Views.MainPage.MainPage" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="fr">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="../Content/CSS.css" />
    <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.0/jquery.min.js"></script>
    <title>Sardinha research</title>
</head>
<body>
    <div id="background">
    </div>
    <header id="header">
        <div id="header-title">
            <h1>Sardinha research</h1>
            <form action="MainPage.aspx" method="post">
                <div id="container-checkbox">
                    <!-- Checkbox -->
                    <div id="container-label-k">
                        <label id="label-K" for="checkbox-K">Recherche sur le K:</label>
                    </div>
                    <div class="wrapper display-inline">
                        <div class="switch_box box_1">
                            <input id="checkbox-K" type="checkbox" name="checkbox-K" class="switch_1" <% if (Request.Form["checkbox-K"] != null)
                                { %>checked<% } %> />
                        </div>
                    </div>
                    <div id="container-label-Web">
                        <label id="label-Web" for="checkbox-Web">Recherche sur le site de l'école</label>
                    </div>
                    <div class="wrapper display-inline">
                        <div class="switch_box box_1">
                            <input id="checkbox-Web" type="checkbox" name="checkbox-Web" class="switch_1" <% if (Request.Form["checkbox-Web"] != null)
                                { %>checked<% } %> />
                        </div>
                    </div>
                </div>
                <!-- Searchbar -->
                <% 
                    if (Request.Form["research"] == null)
                    {
                %>
                <input id="header-input-research" type="text" name="research" placeholder="Mettez ici le mot que vous souhaitez rechercher..." />
                <% 
                    }
                    else
                    {
                %>
                <input id="header-input-research" type="text" name="research" placeholder="Mettez ici le mot que vous souhaitez rechercher..." value="<%=Request.Form["research"] %>" />
                <% 
                    }
                %>
                <input id="submit-button" type="submit" name="submit" value="Rechercher" />
            </form>
        </div>
        <div>
            <div class="element-search-div">
                <!-- Display links -->
                <% 
                    //Verifies if its the first time that the page loads
                    if (Request.Form["research"] != null)
                    {
                        string searchTerm = Request.Form["research"];

                        var arraySearch = searchTerm.Split(' ');
                        List<string> listLinks = GetData(arraySearch);
                        if (listLinks.Count > 0)
                        {
                            foreach (string s in listLinks)
                            {
                                string newS = s.Replace("\\", "/");
                %>
                <h1 class="element-search-div-h1"><a target="_blank" class="element-search-div-h1-a" href="<%if (s[0] != 'h')
                                                      {%>WebForm1.aspx?file=<%}%><%=newS%>"><%=newS%></a></h1>
                <br />
                <br />
                <% 
                        }
                    }
                    else
                    {
                %>
                <h1>0 fichiers trouvés...</h1>
                <% 
                        }
                    }
                %>
            </div>
        </div>
    </header>
</body>
</html>

<script>

    function makeArray(n) {
        this.length = n;
        for (var i = 1; i <= n; i++) {
            this[i] = "";
        }
        return this;
    }
    var i;
    function imagealeatoire() {
        i = Math.floor(6 * Math.random() + 1);
        return image[i];
    }

    image = new makeArray(6);

    image[1] = "images/1.jpg";
    image[2] = "images/2.jpg";
    image[3] = "images/3.png";
    image[4] = "images/4.jpg";
    image[5] = "images/5.png";
    image[6] = "images/6.jpg";

    var test = imagealeatoire();

    var addBackground = document.getElementById("background");

    $(document).ready(function () {
        var test2 = addBackground.style.backgroundImage = "url(\"../" + test + "\")";
    }, 2000);

</script>
