﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Sardinha.Views.MainPage
{
    public partial class MainPage : System.Web.UI.Page
    {
        static SingletonGetData getData;
        protected void Page_Load(object sender, EventArgs e)
        {           
            if (Request.Form["research"] == null)
            {
                getData = SingletonGetData.Instance();
            }               
        }
        protected List<string> GetData(string[] array)
        {
            List<string> tmpList = new List<string>();
            foreach(string s in array)
            {
                tmpList.Add(s);
            }
            bool k = true;
            bool w = true;
            if (Request.Form["checkbox-Web"] == null)
            {
                w = false;
            }
            if (Request.Form["checkbox-K"] == null)
            {
                k = false;
            }
            return getData.GetListOfFiles(tmpList, k, w);
        }
    }
}