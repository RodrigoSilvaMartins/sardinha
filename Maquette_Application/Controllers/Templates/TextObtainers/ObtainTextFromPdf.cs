﻿// ETML
// Auteur: Rodrigo Martins
// Date: 21.05.2018
// Description: Get Text from pdf Template
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Sardinha.Templates.TextObtainers
{
    class ObtainTextFromPdf : TextObtainer
    {
        public ObtainTextFromPdf(FileInfo[] wordFiles, Dictionary<string, Dictionary<string, int>> list) : base(wordFiles, list) { }
        
        /// Source : http://www.c-sharpcorner.com/blogs/reading-contents-from-pdf-word-text-files-in-c-sharp1
        /// <summary>
        /// Gets text from PDF
        /// </summary>
        /// <param name="pDirectory"></param>
        /// <returns></returns>
        protected override string GetText(string pDirectory)
        {
            StringBuilder text = new StringBuilder();
            PdfReader reader = new PdfReader(pDirectory);

            for (int i = 1; i <= reader.NumberOfPages; i++)
            {
                text.Append(PdfTextExtractor.GetTextFromPage(reader, i));
            }
            return text.ToString();
        }
    }
}
