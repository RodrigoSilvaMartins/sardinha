﻿// ETML
// Auteur: Rodrigo Martins
// Date: 21.05.2018
// Description: Get Text from pptx Template
using Microsoft.Office.Core;
using System.Collections.Generic;
using System.IO;

namespace Sardinha.Templates.TextObtainers
{
    class ObtainTextFromPptx : TextObtainer
    {
        public ObtainTextFromPptx(FileInfo[] wordFiles, Dictionary<string, Dictionary<string, int>> list) : base(wordFiles, list) { }
        
        /// <summary>
        /// Gets text from PPTX
        /// </summary>
        /// <param name="pDirectory"></param>
        /// <returns></returns>
        protected override string GetText(string pDirectory)
        {
            Microsoft.Office.Interop.PowerPoint.Application PowerPoint_App = new Microsoft.Office.Interop.PowerPoint.Application();
            Microsoft.Office.Interop.PowerPoint.Presentations multi_presentations = PowerPoint_App.Presentations;
            Microsoft.Office.Interop.PowerPoint.Presentation presentation = multi_presentations.Open(pDirectory, MsoTriState.msoFalse, MsoTriState.msoFalse, MsoTriState.msoFalse);
            string presentation_text = "";
            for (int i = 0; i < presentation.Slides.Count; i++)
            {
                foreach (var item in presentation.Slides[i + 1].Shapes)
                {
                    var shape = (Microsoft.Office.Interop.PowerPoint.Shape)item;
                    if (shape.HasTextFrame == MsoTriState.msoTrue)
                    {
                        if (shape.TextFrame.HasText == MsoTriState.msoTrue)
                        {
                            var textRange = shape.TextFrame.TextRange;
                            var text = textRange.Text;
                            presentation_text += text + " ";
                        }
                    }
                }
            }
            PowerPoint_App.Quit();
            return presentation_text;
        }
    }
}
