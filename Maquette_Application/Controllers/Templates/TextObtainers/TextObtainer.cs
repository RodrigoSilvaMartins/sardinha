﻿// ETML
// Auteur: Rodrigo Martins
// Date: 21.05.2018
// Description: Get Text Template
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Sardinha.Templates.TextObtainers
{
    abstract class TextObtainer
    {
        /// <summary>
        /// Adds the text to a list
        /// </summary>
        /// <param name="wordFiles"></param>
        /// <param name="list"></param>
        public TextObtainer(FileInfo[] wordFiles, Dictionary<string, Dictionary<string, int>> list)
        {
            foreach (FileInfo file in wordFiles)
            {
                if (file.Name.Substring(0, 2) != "~$")
                {
                    string text = GetText(file.FullName);
                    string[] tab_words = Regex.Split(text, "\\s");
                    Dictionary<string, Dictionary<string, int>> dicDirectory = new Dictionary<string, Dictionary<string, int>>();
                    Dictionary<string, int> dicWords = new Dictionary<string, int>();
                    foreach (string s in tab_words)
                    {
                        if (s != "")
                        {
                            if (dicWords.ContainsKey(s))
                            {
                                dicWords[s]++;
                            }
                            else
                            {
                                dicWords.Add(s, 1);
                            }
                        }
                    }
                    list.Add(file.FullName, dicWords);
                }
            }
        }
        /// <summary>
        /// Gets text
        /// </summary>
        /// <param name="pDirectory"></param>
        /// <returns></returns>
        protected abstract string GetText(string pDirectory);
    }
}
