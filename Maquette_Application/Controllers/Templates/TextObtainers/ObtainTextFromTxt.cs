﻿// ETML
// Auteur: Rodrigo Martins
// Date: 21.05.2018
// Description: Get Text from txt Template
using System;
using System.Collections.Generic;
using System.IO;

namespace Sardinha.Templates.TextObtainers
{
    class ObtainTextFromTxt : TextObtainer
    {
        public ObtainTextFromTxt(FileInfo[] wordFiles, Dictionary<string, Dictionary<string, int>> list) : base(wordFiles, list) { }

        /// <summary>
        /// Gets text from txt
        /// </summary>
        /// <param name="pDirectory"></param>
        /// <returns></returns>
        protected override string GetText(string pDirectory)
        {
            return System.IO.File.ReadAllText(Convert.ToString(pDirectory));
        }
    }
}
