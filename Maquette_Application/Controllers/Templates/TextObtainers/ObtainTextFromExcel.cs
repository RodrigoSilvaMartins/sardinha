﻿// ETML
// Auteur: Rodrigo Martins
// Date: 21.05.2018
// Description: Get Text from excel Template
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using Excel = Microsoft.Office.Interop.Excel;

namespace Sardinha.Templates.TextObtainers
{
    class ObtainTextFromExcel : TextObtainer
    {
        public ObtainTextFromExcel(FileInfo[] wordFiles, Dictionary<string, Dictionary<string, int>> list) : base(wordFiles, list)
        {
            WordFiles = wordFiles;
        }

        public FileInfo[] WordFiles { get; }

        /// <summary>
        /// Gets text from excel
        /// </summary>
        /// <param name="pDirectory"></param>
        /// <returns></returns>
        protected override string GetText(string pDirectory)
        {
            string fullText = "";
            //Create COM Objects. Create a COM object for everything that is referenced
            Excel.Application xlApp = new Excel.Application();
            Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(pDirectory);
            List<Excel.Worksheet> listSheets = new List<Excel.Worksheet>();
            foreach (Excel.Worksheet w in xlWorkbook.Sheets)
            {
                listSheets.Add(w);
            }
            for (int i = 0; i < listSheets.Count(); i++)
            {
                Excel._Worksheet xlWorksheet = listSheets[i];
                Excel.Range xlRange = xlWorksheet.UsedRange;

                int rowCount = xlRange.Rows.Count;
                int colCount = xlRange.Columns.Count;

                //iterate over the rows and columns and print to the console as it appears in the file
                //excel is not zero based!!
                for (int p = 1; p <= rowCount; p++)
                {
                    for (int j = 1; j <= colCount; j++)
                    {
                        //add the values to the string
                        if (xlRange.Cells[p, j] != null && xlRange.Cells[p, j].Value2 != null)
                        {
                            fullText += xlRange.Cells[p, j].Value2.ToString() + " ";
                        }
                    }
                }

                //release com objects to fully kill excel process from running in the background
                Marshal.ReleaseComObject(xlRange);
                Marshal.ReleaseComObject(xlWorksheet);
            }
            //cleanup
            GC.Collect();
            GC.WaitForPendingFinalizers();

            //close and release
            xlWorkbook.Close();
            Marshal.ReleaseComObject(xlWorkbook);

            //quit and release
            xlApp.Application.Quit();
            xlApp.Quit();
            Marshal.ReleaseComObject(xlApp);

            return fullText;
        }
    }
}
