﻿// ETML
// Auteur: Rodrigo Martins
// Date: 21.05.2018
// Description: Get Text from docx Template
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Sardinha.Templates.TextObtainers
{
    class ObtainTextFromDocx : TextObtainer
    {
        public ObtainTextFromDocx(FileInfo[] wordFiles, Dictionary<string, Dictionary<string, int>> list) : base(wordFiles, list) { }
        
        /// Source : httrp://www.c-sharpcorner.com/blogs/reading-contents-from-pdf-word-text-files-in-c-sharp1
        /// <summary>  
        /// Reading Text from Word document
        /// </summary>  
        /// <returns>Text</returns>  
        protected override string GetText(string pDirectory)
        {
            StringBuilder text = new StringBuilder();
            Microsoft.Office.Interop.Word.Application word = new Microsoft.Office.Interop.Word.Application();
            object miss = System.Reflection.Missing.Value;
            object path = pDirectory;
            object readOnly = true;
            Microsoft.Office.Interop.Word.Document docs = word.Documents.Open(ref path, ref miss, ref readOnly, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss, ref miss);

            for (int i = 0; i < docs.Paragraphs.Count; i++)
            {
                text.Append(" \r\n " + docs.Paragraphs[i + 1].Range.Text.ToString());
            }
            word.Quit();
            return text.ToString();
        }
    }
}
