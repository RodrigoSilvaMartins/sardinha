﻿// ETML
// Auteur: Rodrigo Martins
// Date: 21.05.2018
// Description: Stoque les données dans cette classe et retourne les données
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Sardinha
{
    class SingletonGetData
    {
        GetTextFactory factory = new GetTextFactory();
        private static volatile SingletonGetData instance;
        private static object syncRot = new Object();
        private const string directory = @"D:\DATA\OK";
        //Dictionaire -> lien du fichier et dictionaire -> mot et numero d'occurences
        private Dictionary<string, Dictionary<string, int>> listTxt = new Dictionary<string, Dictionary<string, int>>();
        private Dictionary<string, Dictionary<string, int>> listPdf = new Dictionary<string, Dictionary<string, int>>();
        private Dictionary<string, Dictionary<string, int>> listDocx = new Dictionary<string, Dictionary<string, int>>();
        private Dictionary<string, Dictionary<string, int>> listXlsx = new Dictionary<string, Dictionary<string, int>>();
        private Dictionary<string, Dictionary<string, int>> listXlsm = new Dictionary<string, Dictionary<string, int>>();
        private Dictionary<string, Dictionary<string, int>> listPptx = new Dictionary<string, Dictionary<string, int>>();
        private Dictionary<string, Dictionary<string, int>> listWeb = new Dictionary<string, Dictionary<string, int>>();

        /// <summary>
        /// Constructor that gets all data in the lists
        /// </summary>
        private SingletonGetData()
        {
            //Variables
            Dictionary<string, Dictionary<string, int>> allLinks = new Dictionary<string, Dictionary<string, int>>();
            webClient client = new webClient();
            Dictionary<string, int> allWords = new Dictionary<string, int>();
            List<string> listAllLinks = new List<string>();
            Links links = new Links();
            DirectoryInfo d = new DirectoryInfo(directory);

            DirectoryInfo[] folderFiles = d.GetDirectories("*"); //Getting Text files
            AddData(d);
            allLinks = links.GetLinksDictionnary("https://www.etml.ch/"); //Gets all links
            listAllLinks = links.GetLinksList(); //Gets list
            foreach (string element in listAllLinks)
            {
                allWords = client.Client(element);

                if (allLinks.ContainsKey(element))
                {
                    if (element.Contains(".etml") || element.Contains("etml-es") || element.Contains("etml."))//Verify if its an etml link
                    {
                            listWeb.Add(element, allWords);                        
                    }
                }
            }
        }
        /// <summary>
        /// Singleton
        /// </summary>
        /// <returns></returns>
        public static SingletonGetData Instance()
        {
            if (instance == null)
            {
                lock (syncRot)
                {
                    instance = new SingletonGetData();
                }

            }
            return instance;
        }
        /// <summary>
        /// Adds the data in the lists
        /// </summary>
        /// <param name="d">list with diretories</param>
        private void AddData(DirectoryInfo d)
        {
            DirectoryInfo[] folderFiles = d.GetDirectories("*"); //Getting Text files
            FileInfo[] pdfFiles = d.GetFiles("*.pdf"); //Getting Text files
            FileInfo[] wordFiles = d.GetFiles("*.docx"); //Getting Text files
            FileInfo[] xlsxFiles = d.GetFiles("*.xlsx");
            FileInfo[] xlsmFiles = d.GetFiles("*.xlsm");
            FileInfo[] pptxFiles = d.GetFiles("*.pptx");
            FileInfo[] txtFiles = d.GetFiles("*.txt"); //Getting Text files

            if (txtFiles.Count() > 0)
            {
                factory.GetText(0, txtFiles, listTxt);
            }
            if (wordFiles.Count() > 0)
            {
                factory.GetText(1, wordFiles, listDocx);
            }
            if (xlsxFiles.Count() > 0)
            {
                factory.GetText(2, xlsxFiles, listXlsx);
            }
            if (xlsmFiles.Count() > 0)
            {
                factory.GetText(2, xlsmFiles, listXlsm);
            }
            if (pdfFiles.Count() > 0)
            {
                factory.GetText(3, pdfFiles, listPdf);
            }
            if (pptxFiles.Count() > 0)
            {
                factory.GetText(4, pptxFiles, listPptx);
            }
            //Parcourit les dossiers et pour chaque appelle la methode avec ce dossié
            if (folderFiles.Count() > 0)
            {
                foreach (DirectoryInfo i in folderFiles)
                {
                    AddData(i);
                }
            }
        }

        /// <summary>
        /// Method that selects the right files and returns a list with the directories
        /// </summary>
        /// <param name="word">list with the searched words</param>
        /// <param name="K">if should search K</param>
        /// <param name="web">if should search web</param>
        /// <returns></returns>
        public List<string> GetListOfFiles(List<string> word,bool K = true, /*int[] array = null*/bool web = true)
        {
            List<int> listSort = new List<int>();
            List<string> listDirectory = new List<string>();
            Dictionary<string, int> dicFiles = new Dictionary<string, int>();
            bool plusExists = false;
            if (word.Count() > 0)
            {
                List<string> listMinusWords = new List<string>();
                List<string> listPlusWords = new List<string>();

                Dictionary<string, Dictionary<string, int>> mainDic = new Dictionary<string, Dictionary<string, int>>();
                                                
                //In case that the user want for exemple only dpf and txt
                if (K)
                {
                    mainDic = mainDic.Union(listTxt).Union(listDocx).Union(listPdf).Union(listPptx).Union(listXlsm).Union(listXlsx).ToDictionary(k => k.Key, v => v.Value);
                }
                if (web)
                {
                    mainDic = mainDic.Union(listWeb).ToDictionary(k => k.Key, v => v.Value);
                }
                if (word.Contains(""))
                {
                    word.Remove("");
                }
                List<string> tWords = new List<string>();
                for (int i = 0; i < word.Count(); i++)
                {
                    tWords.Add(word[i]);
                }
                //Gets all the minus words on a list
                foreach (string minusWord in tWords)
                {
                    if (minusWord.Count() > 0)
                    {
                        if (minusWord[0] == '-')
                        {
                            string newWord = minusWord.Remove(0, 1);
                            listMinusWords.Add(newWord);
                            word.Remove(minusWord);
                        }
                    }
                }
                List<string> teWords = new List<string>();
                for (int i = 0; i < word.Count(); i++)
                {
                    teWords.Add(word[i]);
                }
                //Gets all the plus words on a list
                foreach (string plusWord in teWords)
                {
                    if (plusWord[0] == '+')
                    {
                        plusExists = true;
                        string newWord = plusWord.Remove(0, 1);
                        listPlusWords.Add(newWord);
                        word.Remove(plusWord);
                        word.Add(newWord);
                    }
                }
                bool needsRemove = false;
                //Copy mainlist to a new list
                Dictionary<string, Dictionary<string, int>> tDic = new Dictionary<string, Dictionary<string, int>>();

                for (int i = 0; i < mainDic.Count; i++)
                {
                    tDic.Add(mainDic.ElementAt(i).Key, mainDic.ElementAt(i).Value);
                }
                bool containPlus;
                //Removes all files that dont match the search
                foreach (var v in tDic)
                {
                    containPlus = false;
                    foreach (var t in v.Value)
                    {
                        if (listMinusWords.Contains(t.Key))
                        {
                            needsRemove = true;
                        }


                        if (listPlusWords.Contains(t.Key))
                        {
                            containPlus = true;

                        }

                        if (plusExists == false)
                        {
                            containPlus = true;
                        }
                    }
                    if (needsRemove || !containPlus)
                    {
                        mainDic.Remove(v.Key);
                    }
                    needsRemove = false;
                }
                foreach (var dic in mainDic)
                {
                    foreach (var v in dic.Value)
                    {
                        if (word.Contains(v.Key))
                        {
                            if (!dicFiles.ContainsKey(dic.Key))
                            {
                                dicFiles.Add(dic.Key, v.Value);
                            }
                            else
                            {
                                dicFiles[dic.Key] = dicFiles[dic.Key] + v.Value;
                            }
                        }

                    }
                }
                foreach (var v in dicFiles)
                {
                    listSort.Add(v.Value);
                }
                listSort.Sort();
                int dicCount = dicFiles.Count();
                while (dicFiles.Count() > 0 && dicCount != (dicFiles.Count() + 20))
                {
                    foreach (var v in dicFiles)
                    {
                        if (v.Value > 0)
                        {
                            if (v.Value == listSort.Last())
                            {
                                listDirectory.Add(v.Key);
                                listSort.Remove(listSort.Last());
                                dicFiles.Remove(v.Key);
                                break;
                            }
                        }
                        else
                        {
                            dicFiles.Remove(v.Key);
                            break;
                        }
                    }
                }
            }
            return listDirectory;
        }
        /// <summary>
        /// Reloads the data on the lists
        /// </summary>
        public void ReloadData()
        {
            ResetData();
            DirectoryInfo d = new DirectoryInfo(directory);
            DirectoryInfo[] folderFiles = d.GetDirectories("*"); //Getting Text files
            AddData(d);
        }
        /// <summary>
        /// Resets the data on the lists
        /// </summary>
        private void ResetData()
        {
            listTxt.Clear();
            listPdf.Clear();
            listDocx.Clear();
            listXlsx.Clear();
            listXlsm.Clear();
            listPptx.Clear();
        }
    }
}
