﻿// ETML
// Auteur: Ricardo Cardoso Oliveira
// Date: 12.05.2018
// Description: Récupère tout les mots contenu dans une page html

using System;
using System.Collections.Generic;
using System.Net;
using System.Text.RegularExpressions;

namespace Sardinha
{
    class webClient
    {     
        private WebClient client = new WebClient();
        private Dictionary<string, int> allWordsFromAllLinks = new Dictionary<string, int>();

        public Dictionary<string, int> Client(string url)
        {
            string codePageTags = "";

            Dictionary<string, int> allWords = new Dictionary<string, int>();

            //Obtenir le code source de la page HTML           

            client.Encoding = System.Text.Encoding.UTF8;

            try
            {
                codePageTags = client.DownloadString(url);
            }
            catch
            {
                Console.WriteLine("Type de lien non supporté : " + url + "\n");
            }            

            //Regex qui enlève tout le code HTML, Script, Style ... et ne garde que le text brut de la page
            string codePageTagsLine = Regex.Replace(codePageTags, "\n", "");
            string codePageTagsSytle = Regex.Replace(codePageTagsLine, "<style.+?<\\/style>", "");
            string codePageTagsScript = Regex.Replace(codePageTagsSytle, "<script[^>]*>[\\s\\S]*?<\\/script>", "");
            string codePage = Regex.Replace(codePageTagsScript, "<\\/?[^<]+>", "");            
            string codePageR = Regex.Replace(codePage, "\r", "");
            string codePageT = Regex.Replace(codePageR, "\t", "");
            string codePageSpace = Regex.Replace(codePageT, "  ", "");
            string[] codePageFinal = codePageSpace.Split(' ');

            //Ajouter chaque mot dans une liste
            foreach (string element in codePageFinal)
            {
                if(element != "")
                {
                    if(allWords.ContainsKey(element))
                    {
                        allWords[element]++;                        
                    }
                    else
                    {
                        allWords.Add(element, 1);                        
                    }

                    if(allWordsFromAllLinks.ContainsKey(element))
                    {
                        allWordsFromAllLinks[element]++;
                    }
                    else
                    {
                        allWordsFromAllLinks.Add(element, 1);
                    }
                }
            }

            return allWords;
        }
    }
}
