﻿// ETML
// Auteur: Ricardo Cardoso Oliveira
// Date: 12.05.2018
// Description: Récupère tout les liens contenu dans les balise <a>

using System;
using System.Collections.Generic;
using System.Net;
using System.Text.RegularExpressions;

namespace Sardinha
{
    class Links
    {
        private string codePageLinks;
        private List<string> listLinksHref = new List<string>();
        private List<string> listLinksFinal = new List<string>();
        Dictionary<string, Dictionary<string, int>> allLinks = new Dictionary<string, Dictionary<string, int>>();
        private string addHtmlTag = "";

        /// <summary>
        /// Obtien les liens
        /// </summary>
        /// <param name="url">url</param>
        /// <returns></returns>
        public Dictionary<string, Dictionary<string, int>> GetLinksDictionnary(string url)
        {
            //Obtenir le code source de la page internet
            WebClient client = new WebClient();

            client.Encoding = System.Text.Encoding.UTF8;

            codePageLinks = client.DownloadString(url);

            // Regex et Match
            Regex regex = new Regex("href\\s*=\\s*(?:\"(?<1>[^\"]*)\"|(?<1>\\S+))", RegexOptions.IgnoreCase);
            Regex regexHttp = new Regex("http.*", RegexOptions.IgnoreCase);
            Match match;
            Match matchHttp;

            //Vérification et récupération des liens contenu dans une balise <a>
            for(match = regex.Match(codePageLinks); match.Success; match = match.NextMatch())
            {
                foreach (Group group in match.Groups)
                {                  
                    string test = Convert.ToString(group);

                    string test1 = Regex.Replace(test,"href=", "");

                    string test2 = Regex.Replace(test1, "\"", "");

                    listLinksHref.Add(test2);
                }
            }

            //Récupération des liens qui commancent pas "http"
            foreach (string element in listLinksHref)
            {
                if (element.Contains(".html"))
                {
                    addHtmlTag = "http://etml.ch" + element;
                }

                if (!element.Contains("http://etml.ch") || !element.Contains("https://etml.ch") || !addHtmlTag.Contains("http://etml.ch") || !addHtmlTag.Contains("https://etml.ch"))
                {
                    //string linkElement = "http://etml.ch" + element;
                    if (element.Contains(".etml") || element.Contains("etml.") || element.Contains("etml-es") || addHtmlTag.Contains(".etml") || addHtmlTag.Contains("etml.") || addHtmlTag.Contains("etml-es"))
                    {

                        if (element != "" && allLinks.ContainsKey(element) == false && (element.Contains("http//:") || element.Contains("https://")))
                        {
                            allLinks.Add(element, null);

                            listLinksFinal.Add(element);
                        }

                        if (addHtmlTag != "" && allLinks.ContainsKey(addHtmlTag) == false)
                        {
                            allLinks.Add(addHtmlTag, null);

                            listLinksFinal.Add(addHtmlTag);
                        }
                    }
                }
            }

            //Retourner une liste qui contient tout les liens finaux
            return allLinks;
        }
        /// <summary>
        /// Renvoi la list des links
        /// </summary>
        /// <returns></returns>
        public List<string> GetLinksList()
        {
            return listLinksFinal;
        }
    }
}
