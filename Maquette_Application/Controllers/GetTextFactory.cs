﻿// ETML
// Auteur: Rodrigo Martins
// Date: 21.05.2018
// Description: Factory that sends templates
using System.Collections.Generic;
using System.IO;

namespace Sardinha
{
    class GetTextFactory
    {
        /// <summary>
        /// Gets the templates
        /// </summary>
        /// <param name="key"></param>
        /// <param name="txtFiles"></param>
        /// <param name="list"></param>
        /// <returns></returns>
        public Sardinha.Templates.TextObtainers.TextObtainer GetText(int key, FileInfo[] txtFiles, Dictionary<string, Dictionary<string, int>> list)
        {
            switch (key)
            {
                case 0:
                    return new Sardinha.Templates.TextObtainers.ObtainTextFromTxt(txtFiles, list);
                case 1:
                    return new Sardinha.Templates.TextObtainers.ObtainTextFromDocx(txtFiles, list);
                case 2:
                    return new Sardinha.Templates.TextObtainers.ObtainTextFromExcel(txtFiles, list);
                case 3:
                    return new Sardinha.Templates.TextObtainers.ObtainTextFromPdf(txtFiles, list);
                case 4:
                    return new Sardinha.Templates.TextObtainers.ObtainTextFromPptx(txtFiles, list);
                default:
                    return new Sardinha.Templates.TextObtainers.ObtainTextFromTxt(txtFiles, list);
            }
        }
    }
}
